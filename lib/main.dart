import 'package:flutter/material.dart';
import 'package:metaudea/pages/home_page.dart';

void main() => runApp(MetaUdeA());

class MetaUdeA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MetaUdeA',
      home: HomePage(),
    );
  }
}