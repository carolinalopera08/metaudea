import 'package:flutter/material.dart';

//colors
Color verde1 = Colors.green;
Color grisGeneral = Colors.grey;
Color blancoGen = Colors.white;
Color negro = Colors.black;
Color verdeUdeA = Color(0XFF1e8449);


String nombreApp = 'MetaUdeA';

//textStyles
TextStyle textStyleTextfieldBusqueda(){
  return TextStyle(
    color: negro,
    fontSize: 16,
  );
}

TextStyle textStyleTextfieldBusquedaHint(){
  return TextStyle(
    color: grisGeneral,
    fontSize: 16,
  );
}