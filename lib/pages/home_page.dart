import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metaudea/configuration.dart' as cg;
import 'package:metaudea/widgets/textfileds_widgets.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController textBusquedaContro = TextEditingController();
  TextfieldBusqueda textBusqueda;

  @override
  void initState() {
    // TODO: implement initState
    textBusqueda = TextfieldBusqueda(txtContr: textBusquedaContro);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double ancho = MediaQuery.of(context).size.width;
    double alto = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: cg.verde1,
      body: ListView(
        children: [
          //appBar personalizado
          Container(
            width: ancho,
            height: alto*0.08,
            color: cg.negro,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(onPressed: clickBotonIngre,
                    child: Text('Ingresar', style: TextStyle(color: cg.blancoGen, fontSize: 14),),),
                SizedBox(width: 10,),
                TextButton(onPressed: clickBotonAyuda,
                  child: Text('Ayuda', style: TextStyle(color: cg.blancoGen, fontSize: 14),),),
                SizedBox(width: 20,),
              ],
            ),
          ),
          //container buscador
          Container(
            width: ancho,
            height: alto*0.75,
            color: cg.verdeUdeA,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          color: cg.blancoGen,
                          borderRadius: BorderRadius.circular(16)
                        ),
                        child: Image.asset('assets/logoudea.jpg', height: alto*0.2,)
                    ),
                    SizedBox(width: 10,),
                    Text(cg.nombreApp, style: TextStyle(color: cg.blancoGen, fontSize: 40, fontWeight: FontWeight.bold),),
                  ],
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: ancho*0.30),
                  child: BaseBackgroundTextfields(textBusqueda),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void clickBotonIngre(){

  }
  void clickBotonAyuda(){

  }

}
