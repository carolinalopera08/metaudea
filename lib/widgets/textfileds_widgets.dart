import 'package:flutter/material.dart';
import 'package:metaudea/configuration.dart' as cg;

class TextfieldBusqueda extends StatelessWidget {
  TextfieldBusqueda({
    @required this.txtContr,
  });
  TextEditingController txtContr;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: txtContr,
      maxLength: 80,
      style: cg.textStyleTextfieldBusqueda(),
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        errorStyle: TextStyle(color: cg.blancoGen),
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        contentPadding:
        EdgeInsets.only(left: 15, bottom: 11, top: 15, right: 15),
        //prefixIcon: Icon(Icons.search, color: cg.greyDarkColor,),
        prefixIcon: Icon(Icons.search, color: cg.blancoGen,),
        counterText: '',
        hintText:'Buscar metáfora o expresión metafórica',
        hintStyle: cg.textStyleTextfieldBusquedaHint(),
      ),
    );
  }
}

class BaseBackgroundTextfields extends StatelessWidget {

  BaseBackgroundTextfields(this.textGet);

  Widget textGet;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(1.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: cg.blancoGen.withOpacity(0.9)),
            color: cg.blancoGen.withOpacity(0.6)
        ),
        child: Center(child: textGet),
      ),
    );
  }
}